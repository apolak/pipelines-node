FROM node:8.9.4

WORKDIR /app
ADD . /app

RUN npm i

EXPOSE 3000

CMD node index.js