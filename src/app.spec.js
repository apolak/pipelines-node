const app = require('./app');
const request = require('supertest');

describe("App", () => {
  it('return something', () => {
    return request(app)
      .get('/')
      .expect(200)
      .then(res => {
        expect(res.body).toEqual({
          someParam: 'value'
        })
      });
  });

  it('handles errors', () => {
    return request(app)
      .get('/asdasd')
      .expect(404)
      .then(res => {
        expect(res.text).toEqual('Sorry can\'t find that!')
      });
  })
});