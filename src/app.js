const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());

app.get('/', (req, res) => {
  res.json({
    someParam: 'value'
  })
});

app.use((req, res, next) => {
  res
    .status(404)
    .send("Sorry can't find that!")
});

app.use((err, req, res, next) => {
  res
    .status(400)
    .send({
      err
    })
});

module.exports = app;
